Select y.Fakturert,b.value [Budsjett],b.month from score_card_budgetline b left outer join( 
SELECT CONVERT(int,SUM(fakturertbelop)/1000) [Fakturert],maaned from(
SELECT Aktivitet.AktivitetID, Aktivitet.AktivitetType, Aktivitet.Kundenummer, Aktivitet.Selger, Aktivitet.Dato, Month(Aktivitet.Dato) AS 'Maaned', Day(Aktivitet.Dato) AS 'Dag', Aktivitet.AvtaleNummer, Aktivitet.AntallTimer, Aktivitet.BelastesKunde, Aktivitet.Godkjent, Aktivitet.TimePris, Aktivitet.AntallTimerFakturert, Kunde.KundeNavn, Aktivitet.AktivitetSubtype, (Aktivitet.AntallTimerFakturert*Aktivitet.Timepris) AS 'fakturertbelop', (Aktivitet.AntallTimer*Aktivitet.Timepris) AS 'loggetbelop', (ISNULL(Aktivitet.ordrenummer,0)+ISNULL(Aktivitet.Timepulje,0)) AS 'OrdrePulje', Aktivitet.MedTilFakturering, (Aktivitet.AntallTimer-ISNULL(Aktivitet.AntallTimerFakturert,0)) AS 'Diff', (Aktivitet.AntallTimer/8) AS 'AntallDager', (Aktivitet.AntallTimerFakturert/8) AS 'AntallDagerFakturert', Avtale.Produkt AS 'Prosjektnavn', 
CAST(
             CASE 
                WHEN Aktivitet.Timepulje < 100000 AND Aktivitet.timepulje>0
                    THEN 'Pulje'
				WHEN Aktivitet.ordrenummer > 100000 
					THEN 'Fakturert'
                  ELSE 'Blank'
            END AS varchar(10)) as 'FakturertSom',
CAST(
             CASE 
                WHEN (Avtale.Produkt LIKE '%support point%' OR Avtale.Produkt LIKE '%supportpoint%' OR Avtale.Produkt LIKE '%pureservice%' OR Avtale.Produkt LIKE '%PS%') AND (Avtale.produkt LIKE '%installasjon%' OR Avtale.produkt LIKE '%installere%' OR Avtale.produkt LIKE '%implementering%')
                    THEN 'PureserviceInstall'
				ELSE 'Other'
            END AS varchar(20)) as 'PureInstall'

		 
FROM contact.dbo.Aktivitet Aktivitet, contact.dbo.Avtale Avtale, contact.dbo.Kunde Kunde
WHERE Aktivitet.Kundenummer = Kunde.KundeNummer AND Aktivitet.AvtaleNummer = Avtale.AvtaleNummer AND ((Kunde.Region='Norway') AND (Aktivitet.Dato>{ts '2016-01-01 00:00:00'} And Aktivitet.Dato<{ts '2017-01-01 00:00:00'}))
) x group by maaned) y on (b.month=y.Maaned)
where b.item='Consulting' and b.year=2016
order by month