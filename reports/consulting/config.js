config = {
    chartConfig: {
        nameCols: ['Fakturert', 'Budsjett'],
        categoryCol: 'month',
        categoryIsMonth: true,
        title: 'Fakturerte timer leveranse YTD',
        stacking: false,
        accumulated: true,
        series:{
            'Budsjett':{
                type:'spline'
            }
        }
    },

    dataplacement: 'right',
    datatableConfig: {
    	searching:false,
        order: [],
        columns: [{
            "data": function(row,type,val,meta){
                if(type="data"){
                    return moment.months(row['month']-1);
                }
            },
            "title": "Måned"
        }, {
            "data": "Fakturert",
            "title": "Fakturert",
        }, {
            "data": "Budsjett",
            "title": "Budsjett"
        }],

    }

}
module.exports = config;