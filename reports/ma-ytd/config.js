config = {
    chartConfig: {
            nameCol: 'Selger',
            valueCol: 'Beløp',
            title: 'Selgers andel av årstotalen',
            seriesName:"Solgt"
        }

}
module.exports = config;