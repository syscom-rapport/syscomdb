config = {
    /*imgurls: ['http://localhost:12345/image/1', 'http://localhost:12345/image/2', 'http://localhost:12345/image/3', 'http://localhost:12345/image/4', 'http://localhost:12345/image/5',
        'http://localhost:12345/image/6', 'http://localhost:12345/image/7'
    ]*/
    rootimgurl:'http://localhost:12345/image/',
    imagelisturl:'http://localhost:12345/list/'
    ,
    slickConfig: {
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2500,
        arrows: false
    }

}
module.exports = config;