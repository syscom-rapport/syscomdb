config = {
    chartConfig: {
            nameCol: 'Selger',
            valueCols: ['Usikker','FC','Solgt'],
            title: 'Forecast',
            seriesName:"Solgt"
            ,hcConfig:{
            	colors:['#7cb5ec','#434348','#90ed7d']
            }
        }

}
module.exports = config;