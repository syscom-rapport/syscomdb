config = {
	pivotincludeCount: true,
    hideUI: false,
    chartConfig: {
        hcConfig: {
            yAxis: {
                plotBands: [{
                    from: 5,
                    to: 15,
                    color: 'rgba(255, 51, 51, 0.2)',
                    label: {
                        text: 'Mer enn 50% ledig',
                        style: {
                            color: '#fff'
                        },
                        align:"right"

                    }
                }]
            }
        }
    },
    sort: "-days"

}
module.exports = config;