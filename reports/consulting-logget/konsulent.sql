select selger, SUM(antallTimer) as timer, SUM(antalltimer * timepris) as kroner, u.fulltnavn from aktivitet a inner join userlogin u on a.selger = u.loginid 
where MONTH(dato) = MONTH(getdate()) and YEAR(dato) = YEAR(GETDATE()) and u.region = 'norway'
and a.AktivitetSubtype = 'Project'
group by selger, fulltnavn having sum(antalltimer * timepris) > 0  order by kroner desc