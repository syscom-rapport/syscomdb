config = {
    chartConfig: {
        nameCols: ['kroner', 'timer'],
        categoryCol: 'fulltnavn',
        categoryIsMonth: false,
        title: 'Loggede timer/kroner pr konsulent denne måned.',
        stacking: false,
        accumulated: false,
        series: {
            "timer": {
                yAxis: 1
            }
        },
        hcConfig: {
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kr'
                },
                title: {
                    text: 'Verdi'
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Antall timer'
                },
                labels: {
                    format: '{value} timer'
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            }
        }
    },

    dataplacement: 'right',
    datatableConfig: {
        searching: false,
        order: [],
        columns: [{
            "data": "fulltnavn",
            "title": "Konsulent"
        }, {
            "data": "kroner",
            "title": "Kroner",
        }, {
            "data": "timer",
            "title": "Timer"
        }]
    }

}
module.exports = config;