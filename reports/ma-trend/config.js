config = {
    chartConfig: {
        nameCol: 'Navn',
        valueCols: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
        title: 'Trend',
        seriesName: "Solgt"
    }

}
module.exports = config;