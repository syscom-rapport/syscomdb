var directionArray = ["up", "down"];
var rounds = 2
var visibleItem = 0;

function setupRotation(callback) {
    setTimeout(setup, 0);
     callback();
}

function setup() {
    $('.sr-rotate').each(function(key) {
        itemarray.push("#" + $(this).attr('id'));
    });
    for (var i = 1; i < itemarray.length; i++) {
        $(itemarray[i]).hide();
    }
    setTimeout(setNextvisible, 20000);
}

function addImages(callback) {
    var rpname = $('[data-imagelist]').data('imagelist');
    var repconfig = rpname ? reportconfig[rpname] : reportconfig;
    var rootimgurl = repconfig.rootimgurl;
    var imagelisturl = repconfig.imagelisturl;
    imgurls = [];
    $.getJSON(imagelisturl, function(data) {
        $.each(data, function(key, val) {
            imgurls.push(rootimgurl + key);
        });
        var imageholder = $('#images');
        for (var i = 0; i < imgurls.length; i++) {
            imageholder.append('<div class="sr-rotate row" id="imghh' + i.toString() + '"><div class="large-12 medium-12 small-12 columns"><img src="' + imgurls[i] + '" style="height:1000px;width:auto;" /></div></div>');
        }
        callback();
    });

}
/*.row(style="max-width:none;") 
   .large-12.medium-12.small-12.columns*/
function setNextvisible() {
    var effect = getEffect();
    $(itemarray[visibleItem++]).hide(effect.name, effect);
    if (visibleItem == itemarray.length) {
        rounds--;
        if (rounds == 0)
            location.reload();
        else
            visibleItem = 0;
    }
    $(itemarray[visibleItem]).show(effect.name, effect);
    setTimeout(setNextvisible, 20000);

}

function getEffect() {
    //var direction = directionArray[Math.floor(Math.random() * 2)];
    return {
        direction: "down",
        name: "blind",
        duration: 1000
    };

}
$(document).ready(function() {
    async.series([

        function(cb) {
            addImages(cb)
        },
        function(cb) {
            setupRotation(cb)
        }
    ]);
});